# Advanced Git Tutorial #

## Running the tutorial ##

### Linux ###
* Install tmux and git.
* Clone the repository:
```git clone https://liorgol@bitbucket.org/liorgol/git_tutorial.git```
* cd into the exercises directory:
```cd git_tutorial/exercises```
* Run `./go`.

## Hints ##
In some of the exercises there is a `hint` file which gives references to git commands you should be familiar with to solve the exercise. Even if you can solve the exercise without looking at this file, it is recommended to check it afterwards and make sure you are familiar with the commands it mentions.
To view the hint file, simply run: `cat hint`.

## Restarting an exercise ##
Whenever you start an exercise using the `exercise/go`  script, it starts from scratch.
This has an upside and a downside: the upside is that if you did something in the exercise that you don't know how to fix, you can exit and restart the exercise. On the other hand, it means that if you exit the exercise before you completed it, you won't be able to return to it using the `exercise/go`  script. This is usually not a problem as the exercises are relatively short.
